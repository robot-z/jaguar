/*
 * Copyright (c) 2017 Ericsson India Global Services Pvt Ltd. and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.future.listener;

import com.google.common.util.concurrent.ListenableFuture;
import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.md.sal.binding.api.DataObjectModification;
import org.opendaylight.controller.md.sal.binding.api.DataTreeChangeListener;
import org.opendaylight.controller.md.sal.binding.api.DataTreeIdentifier;
import org.opendaylight.controller.md.sal.binding.api.DataTreeModification;
import org.opendaylight.controller.md.sal.binding.api.WriteTransaction;
import org.opendaylight.controller.md.sal.common.api.data.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.yang.ietf.inet.types.rev130715.IpAddress;
import org.opendaylight.yang.gen.v1.urn.opendaylight.coe.northbound.k8s.node.rev170829.K8sNodesInfo;
import org.opendaylight.yang.gen.v1.urn.opendaylight.coe.northbound.k8s.node.rev170829.k8s.nodes.info.K8sNodes;
import org.opendaylight.yang.gen.v1.urn.opendaylight.coe.northbound.pod.rev170611.pod_attributes.Interface;
import org.opendaylight.yangtools.concepts.ListenerRegistration;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

public class NodeListener implements DataTreeChangeListener<K8sNodes> {

    private static final Logger LOG = LoggerFactory.getLogger(NodeListener.class);
    private ListenerRegistration<NodeListener> listenerRegistration;
    private final DataBroker dataBroker;
    //private final JobCoordinator jobCoordinator;

    public NodeListener(final DataBroker dataBroker) {
        registerListener(LogicalDatastoreType.CONFIGURATION, dataBroker);
        this.dataBroker = dataBroker;
    }

    protected InstanceIdentifier<K8sNodes> getWildCardPath() {
        return InstanceIdentifier.create(K8sNodesInfo.class).child(K8sNodes.class);
    }

    public void registerListener(LogicalDatastoreType dsType, final DataBroker db) {
        final DataTreeIdentifier<K8sNodes> treeId = new DataTreeIdentifier<>(dsType, getWildCardPath());
        listenerRegistration = db.registerDataTreeChangeListener(treeId, NodeListener.this);
    }

    public void close() {
        if (listenerRegistration != null) {
            try {
                listenerRegistration.close();
            } finally {
                listenerRegistration = null;
            }
        }
    }

    @Override
    public void onDataTreeChanged(@Nonnull Collection<DataTreeModification<K8sNodes>> changes) {
        for (DataTreeModification<K8sNodes> change : changes) {
            final DataObjectModification<K8sNodes> mod = change.getRootNode();

            switch (mod.getModificationType()) {
                case DELETE:
                    delete(mod.getDataBefore());
                    break;
                case SUBTREE_MODIFIED:
                    update(mod.getDataBefore(), mod.getDataAfter());
                    break;
                case WRITE:
                    if (mod.getDataBefore() == null) {
                        add(mod.getDataAfter());
                    } else {
                        update(mod.getDataBefore(), mod.getDataAfter());
                    }
                    break;
                default:
                    LOG.error("Unhandled modification type " + mod.getModificationType());
                    break;
            }
        }
    }

    private void add(K8sNodes nodesNew) {
        IpAddress ipAddress = nodesNew.getInternalIpAddress();

    }

    private void update(K8sNodes podsOld, K8sNodes podsNew) {
        // TODO
    }

    private void delete(K8sNodes podsOld) {
        // TODO
    }

    private class RendererConfigAddWorker implements Callable<List<ListenableFuture<Void>>> {
        String podInterfaceName;
        Interface podInterface;

        RendererConfigAddWorker(String podInterfaceName, Interface podInterface) {
            this.podInterfaceName = podInterfaceName;
            this.podInterface = podInterface;
        }

        @Override
        public List<ListenableFuture<Void>> call() {
            LOG.trace("Adding Pod : {}", podInterface);
            WriteTransaction wrtConfigTxn = dataBroker.newWriteOnlyTransaction();
            //CoeUtils.createElanInstanceForTheFirstPodInTheNetwork(podInterface, wrtConfigTxn, dataBroker);
            LOG.info("interface creation for pod {}", podInterfaceName);
            //String portInterfaceName = CoeUtils.createOfPortInterface(podInterface, wrtConfigTxn, dataBroker);
            LOG.debug("Creating ELAN Interface for pod {}", podInterfaceName);
            //CoeUtils.createElanInterface(podInterface, portInterfaceName, wrtConfigTxn);
            return Collections.singletonList(wrtConfigTxn.submit());
        }
    }
}