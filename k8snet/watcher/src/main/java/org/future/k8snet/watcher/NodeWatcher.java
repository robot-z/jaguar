/*
 * Copyright (c) 2017 Future Network. and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package org.future.k8snet.watcher;

import com.coreos.jetcd.Client;
import com.coreos.jetcd.KV;
import com.coreos.jetcd.Watch;
import com.coreos.jetcd.data.ByteSequence;
import com.coreos.jetcd.kv.GetResponse;
import com.coreos.jetcd.watch.WatchEvent;
import com.coreos.jetcd.watch.WatchResponse;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
import java.util.concurrent.ExecutionException;
import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NodeWatcher {
    private static final Logger LOG = LoggerFactory.getLogger(NodeWatcher.class);

    private final DataBroker dataBroker;
    private String endpoints = "http://10.42.118.50:2379";
    Client client = null;

    public NodeWatcher(final DataBroker dataBroker) {
        this.dataBroker = dataBroker;
        LOG.info("new NodeWatcher");
        //listKeys();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    LOG.info("",e);
                }
                listKeys();
            }
        }).start();
    }

    public void listKeys() {
        client = Client.builder().endpoints(endpoints).build();
        KV kvClient = client.getKVClient();
        GetResponse getResponse = null;
        try {
            getResponse = kvClient.get(ByteSequence.fromString("/")).get();
            if (getResponse.getKvs().isEmpty()) {
                // key does not exist
                return;
            }
            LOG.info("/");
            LOG.info("value count=" + getResponse.getKvs().size());
            LOG.info(getResponse.getKvs().get(0).getValue().toStringUtf8());
        } catch (InterruptedException e) {
            LOG.info("",e);
        } catch (ExecutionException e) {
            LOG.info("",e);
            if (kvClient != null) {
                kvClient.close();
            }
            if (client != null) {
                client.close();
            }
        }
    }

    public void watcher() {

        Watch watch = null;
        Watch.Watcher watcher = null;

        try {
            client = Client.builder().endpoints(endpoints).build();
            watch = client.getWatchClient();
            watcher = watch.watch(ByteSequence.fromString("test_key"));
//
//            for (int i = 0; i < cmd.maxEvents; i++) {
//                LOGGER.info("Watching for key={}", cmd.key);
            WatchResponse response = watcher.listen();
//
            for (WatchEvent event : response.getEvents()) {
//                    LOG.info("type={}, key={}, value={}",
//                            event.getEventType(),
//                            Optional.ofNullable(event.getKeyValue().getKey())
//                                    .map(ByteSequence::toStringUtf8)
//                                    .orElse(""),
//                            Optional.ofNullable(event.getKeyValue().getValue())
//                                    .map(ByteSequence::toStringUtf8)
//                                    .orElse("")
//                    );
            }
//            }
        } catch (InterruptedException e) {
            LOG.info("",e);
            if (watcher != null) {
                watcher.close();
            }

            if (watch != null) {
                watch.close();
            }

            if (client != null) {
                client.close();
            }
        }
    }

    public void close() {
        if (client != null) {
            client.close();
        }
    }
}
